#include <stdio.h>
#include <stdlib.h>

//Ejercicio Agencia Viajes - Lista enlazada, Colas - Memoria Din�mica

//Tipo dato para cola de plazas
typedef struct nCola{
	int num;
	struct nCola *sig;
} plazas;

//Tipo dato para la lista ordenada de asignaci�n de plazas
typedef struct nodo{
	char *nombre;
	struct nCola *plazasCliente;
	struct nodo *sig;
} plaza;

int pintaMenu();
plaza* anadirPlazaLista(plaza *lista, plaza *nueva);
plaza* crearReserva(int numPlaza);
void verClientes(plaza *lista);
plazas* iniciarCola();
plazas* guardarEnCola(plazas *cola, int num);
int sacarDeCola(plazas **cola);
void verPlazas(plazas *cola);
plaza* eliminarCliente(plaza *lista, plazas *cola);
void buscarPorPlaza(plaza *lista);


int main(int argc, char *argv[]) {
	int opc, numPlaza;
	plaza *lista = NULL;
	plaza *nueva;	
	plazas *colaPlazas = iniciarCola();
	
	do{
		opc = pintaMenu();
		switch (opc){
			case 1:
				numPlaza = sacarDeCola(&colaPlazas);
				nueva = crearReserva(numPlaza);
				lista = anadirPlazaLista(lista,nueva);
				break;
			case 2:
				lista = eliminarCliente(lista, colaPlazas);
				break;
			case 3:
				break;
			case 4:
				buscarPorPlaza(lista);
				break;
			case 5:
				verClientes(lista);
				break;
			case 6:
				printf("\nPlazas libres: \n");
				verPlazas(colaPlazas);
				break;
			case 7:
				printf("Adios");
				break;
		}
		
	}while (opc!=7);
	return 0;
}

void buscarPorPlaza(plaza *lista){
	int num;
	
	printf("\nDime el n�mero de plaza a buscar ");
	scanf("%i", &num);
	while (lista!=NULL){
		while (lista->plazasCliente != NULL){
			if (lista->plazasCliente->num == num){
				printf("\nLa plaza %i la tiene reservada el cliente %s",num,lista->nombre);
				return;	
			}
			lista->plazasCliente = lista->plazasCliente->sig;
		}
		lista= lista->sig;
	}
	printf("\nPlaza no encontrada");
}

plaza* eliminarCliente(plaza *lista, plazas *cola){
	plaza *ant, *aux;
	int i;
	char *nombre;
	if (lista==NULL){
		printf("No hay reservas para eliminar");
		return NULL;
	}
	printf("\nEscribe el nombre del cliente a borrar ");
	scanf("%s", nombre);
	aux = lista;
	while(strcmp(aux->nombre,nombre)!=0){
		ant = aux;
		aux = aux->sig;
		if (aux==NULL){
			printf("\nNo se ha encontrado el nombre");
			return lista;
		}
	}
	if (ant == aux){
		lista = NULL;
		while (aux->plazasCliente != NULL){
			guardarEnCola(cola,aux->plazasCliente->num);
			aux->plazasCliente= aux->plazasCliente->sig;
		}
		free(aux);
		return lista;
	}
	ant->sig = aux->sig;
	while (aux->plazasCliente != NULL){
			guardarEnCola(cola,aux->plazasCliente->num);
			aux->plazasCliente= aux->plazasCliente->sig;
		}
	free(aux);
	return lista;
}

plaza* anadirPlazaLista(plaza *lista, plaza *nueva){
	plaza *ant = lista;
	plaza *aux = lista;
	
	if (lista==NULL){
		return nueva;
	}
	//Recorro la lista para insertar en orden en ella el nuevo elemento
	//Si encuentro el nombre que ya existe le a�ado una nueva plaza en su vector de plazas
	while(aux != NULL){
		//Si el nuevo es menor, lo inserto antes
		if (strcmp(aux->nombre,nueva->nombre) > 0){
			//Hay un �nico elemento
			if (aux->sig == NULL && lista==aux){
				nueva->sig = lista;
				lista = nueva;
				return lista;
			}
			//Hay mas de un elemento
			nueva->sig = ant->sig;
			ant->sig = nueva;
			return lista;
		}
		
		//El nuevo es el mayor de todos
		if (strcmp(aux->nombre,nueva->nombre) < 0 && aux->sig==NULL){
			aux->sig = nueva;
			return lista;
		}
		
		//Si ya existe el nombre le a�ado la plaza a su vector de plazas
		if (strcmp(aux->nombre,nueva->nombre) == 0){
			guardarEnCola(aux->plazasCliente, nueva->plazasCliente->num);
			free(nueva);
			return lista;
		}
		ant = aux;
		aux = aux->sig;
	}
	return lista;
	
}


int sacarDeCola(plazas **cola){
	int sacar;
	sacar = (*cola)->num;
	*cola = (*cola)->sig;
	return sacar;
}

plazas* guardarEnCola(plazas *cola, int num){
	plazas *aux;
	//Cola vacia
	plazas *nueva = malloc(sizeof(plaza));
	nueva->num = num;
	nueva->sig = NULL;
	if (cola==NULL){
		cola = nueva;
		return cola;
	}
	aux = cola;
	while(aux->sig!=NULL){
		aux = aux->sig;
	}
	//Me coloco al final de la cola e inserto elemento
	aux->sig = nueva;
	return cola;
}

plazas* iniciarCola(){
	int i;
	plazas *cola = NULL;
	for (i=1 ;i<=1000; i++){
		cola = guardarEnCola(cola,i);
	}
	return cola;
}

void verPlazas(plazas *plazas){
	while(plazas!=NULL){
		printf("%i\n",plazas->num);
		plazas=plazas->sig;
	}
}

void verClientes(plaza *lista){
	if (lista==NULL){
		printf("\nNo hay reservas realizadas\n");
	}else{
		printf("\nReservas realizadas:\n");
	}
	plazas *pCliente;
	while(lista!=NULL){
		printf("\nCliente: %s:  -- Plazas ocupadas: ", lista->nombre);
		pCliente = lista->plazasCliente;
		while(pCliente!=NULL){
			printf("- %i ", pCliente->num);
			pCliente = pCliente->sig;
		}
		lista = lista->sig;
	}
}

plaza* crearReserva(int numPlaza){
	plaza *nueva = malloc(sizeof(plaza));
	printf("Escriba el nombre del cliente ");
	nueva->nombre = malloc(sizeof(char)*20);
	scanf("%s", nueva->nombre);
	nueva->plazasCliente = malloc(sizeof(plazas));
	nueva->plazasCliente->num = numPlaza; //Hay que a�adir la cola de reservas automaticamente
	nueva->plazasCliente->sig = NULL;
	nueva->sig = NULL;
	return nueva;
}

int pintaMenu(){
	int opc=0;
	do{
		printf("\n\n******** Men� Agencia Viajes **********\n");
		printf("1- Reservar plaza a un cliente\n");
		printf("2- Liberar plaza de un cliente\n");
		printf("3- Consultar plazas de un cliente\n");
		printf("4- Consultar nombre de un cliente que tiene una plaza\n");
		printf("5- Consultar todos los clientes con sus plazas\n");
		printf("6- Consultar plazas libres\n");
		printf("7- Salir\n");
		scanf("%i", &opc);
	}while(opc<1 || opc>7);
	return opc;
}
